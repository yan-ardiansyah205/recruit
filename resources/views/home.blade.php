@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button type="button" data-toggle="modal" data-target="#New" class="btn float-right" style="margin-top: 20px">Add New</button>
                    <h3>Dashboard</h3>

                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                        <table id="data" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Stok</th>
                                @if(\Illuminate\Support\Facades\Auth::guard('admin')->check() || \Illuminate\Support\Facades\Auth::guard('owner')->check())
                                    <th>Harga Jual</th>
                                @endif
                                @if(\Illuminate\Support\Facades\Auth::guard('owner')->check())
                                    <th>Harga Beli</th>
                                @endif
                                <th>action</th>
                            </tr>
                            </thead>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="Edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="heading"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    X
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('barangs.edit')}}" method="post" files=true enctype="multipart/form-data" class="form-horizontal">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Kode</label>
                        <div class="col-12">
                            <input type="number" id="kode" class="form-control" name="kode" placeholder="Enter Code" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Nama</label>
                        <div class="col-12">
                            <input type="text" id="nama" class="form-control" name="nama" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Stok</label>
                        <div class="col-12">
                            <input type="number" id="stok" class="form-control" name="stok" placeholder="Enter Stock" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Harga Jual</label>
                        <div class="col-12">
                            <input type="number" id="jual" class="form-control" name="jual" placeholder="Enter Price" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Harga Beli</label>
                        <div class="col-12">
                            <input type="number" id="beli" class="form-control" name="beli" placeholder="Enter Price" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Save changes</button>
                        <button data-dismiss="modal" class="btn btn-danger" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="New" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" >New Barang </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    X
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('barang')}}" method="post" files=true class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Kode</label>
                        <div class="col-12">
                            <input type="number" class="form-control" name="kode" placeholder="Enter Code" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Nama</label>
                        <div class="col-12">
                            <input type="text" class="form-control" name="nama" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Stok</label>
                        <div class="col-12">
                            <input type="number" class="form-control" name="stok" placeholder="Enter Stock" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Harga Jual</label>
                        <div class="col-12">
                            <input type="number" class="form-control" name="jual" placeholder="Enter Price" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-12 control-label">Harga Beli</label>
                        <div class="col-12">
                            <input type="number" class="form-control" name="beli" placeholder="Enter Price" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Save changes</button>
                        <button data-dismiss="modal" class="btn btn-danger" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
    @parent
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">--}}
    <style>
        .form-horizontal .control-label{
            text-align: left!important;
        }
    </style>
@endsection

@section('javascript')
    @parent
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    {{--<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>--}}
    <script>
        $('#data').DataTable( {
            @if(\Illuminate\Support\Facades\Auth::guard('owner')->check())
            "ajax": "{{route('data.owner')}}",
            @elseif(\Illuminate\Support\Facades\Auth::guard('admin')->check())
            "ajax": "{{route('data.admin')}}",
            @else
            "ajax": "{{route('data.staff')}}",
            @endif

            "columns": [
                { "data": "kode" },
                { "data": "nama" },
                { "data": "stok" },
                @if(\Illuminate\Support\Facades\Auth::guard('admin')->check() || \Illuminate\Support\Facades\Auth::guard('owner')->check())
                { "data": "harga_jual" },
                @endif
                @if(\Illuminate\Support\Facades\Auth::guard('owner')->check())
                { "data": "harga_beli" },
                @endif
                { sortable: false, "render": function(data, type, full, meta) {
                        var id = full.id;
                        return '<form method="POST" action="{{route('barangs.delete')}}">' +
                            '{{ csrf_field() }}' +
                            '{{ method_field('DELETE') }}' +
                            '<input type="hidden" name="id" value="'+id+'">'+
                            '<div class="form-group">' +
                            '<a href="javascript:void(0)" data-id="'+id+'" class="btn btn-sm Edit-btn btn-warning" style="margin-right:10px">edit</a>'+
                            '<button type="submit" class="btn btn-danger btn-sm delete-user" value="Delete user">Delete</button>' +
                            '</div>' +
                            '</form>';
                    }
                }
            ]
        } );
    </script>
    <script>
        $('.container').on('click', '.Edit-btn', function () {
            var id = $(this).data('id');
            $.get('{{url('barang')}}/'+id, function (data) {
                $('#heading').html('Edit '+data.nama);
                $('#Edit').modal('toggle');
                $('#id').val(data.id);
                $('#kode').val(data.kode);
                $('#nama').val(data.nama);
                $('#stok').val(data.stok);
                $('#jual').val(data.harga_jual);
                $('#beli').val(data.harga_beli);
            })
        });
    </script>
@endsection
