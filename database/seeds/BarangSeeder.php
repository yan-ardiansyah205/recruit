<?php

use Illuminate\Database\Seeder;
use Faker\Provider\Base;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 10000; $i++) {
            \App\Barang::create([
                'kode' => $faker->ean13,
                'nama' => $faker->name,
                'stok' => $faker->numberBetween($min = 1, $max = 1000),
                'harga_jual' => $faker->numerify('####000'),
                'harga_beli' => $faker->numerify('####000'),
            ]);
        }
    }
}
