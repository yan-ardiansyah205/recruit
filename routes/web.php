<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home/admin', 'HomeController@index')->name('home.admin')->middleware('auth:admin');
Route::get('/home/staff', 'HomeController@index')->name('home.staff')->middleware('auth:staff');
Route::get('/home/owner', 'HomeController@index')->name('home.owner')->middleware('auth:owner');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');

Route::post('login', 'Auth\LoginController@requestLogin')->name('request_login');

Route::post('register', 'Auth\RegisterController@createRegister');

Route::get('/data/admin', 'Api\ProductController@index')->name('data.admin')->middleware('auth:admin');
Route::get('/data/staff', 'Api\ProductController@index')->name('data.staff')->middleware('auth:staff');
Route::get('/data/owner', 'Api\ProductController@index')->name('data.owner')->middleware('auth:owner');

Route::post('barang', 'BarangController@post')->name('barang');
Route::put('barang', 'BarangController@edit')->name('barangs.edit');
Route::delete('barang', 'BarangController@delete')->name('barangs.delete');
Route::get('barang/{id}', 'BarangController@getBarang')->name('barangs');