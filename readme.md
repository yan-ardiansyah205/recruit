### Pertanyaan

1. Apa yang kamu ketahui tentang pekerjaan Software Engineer di dunia startup khususnya bidang SaaS?
2. Sebutkan project / tugas yang pernah dibuat menggunakan framework NodeJS maupun Laravel berikut dengan arsitektur jaringan & framework yang dipakai.

### Jawaban

1. SaaS adalah Software as a service dimana software engineer sangat berperan dalam pengembangan software dalam dunia startup
2. Project yang pernah di kerjakan:
- [Tabung Haji Umrah](https://www.tabunghajiumroh.com/) sebagai frontend programmer mebggunakan framework laravel
- Tabung Haji Umrah (Sistem Admin) sebagai frontend programmer menggunakan framework laravel
- [Lactashare](http://lactashare.id/) sebagai fullstack programmer menggunakan framework laravel
- AksesToko Semen Indonesia sebagai pengembang software menggunakan framework CI
- SukmaSinergi sebagai frontend programmer menggunakan framework laravel
