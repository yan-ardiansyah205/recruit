<?php

namespace App\Http\Controllers;

use App\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BarangController extends Controller
{

    public function post(Request $request)
    {
        if((Auth::guard('owner')->check()||Auth::guard('admin')->check()||Auth::guard('staff')->check()) == false){
            return redirect()->back();
        };
//        dd($request->all());
        Barang::create([
            'kode'=>$request->kode,
            'nama'=>$request->nama,
            'stok'=>$request->stok,
            'harga_jual'=>$request->jual,
            'harga_beli'=>$request->beli
        ]);

        return redirect()->back();
    }
    public function getBarang($id){

        if((Auth::guard('owner')->check()||Auth::guard('admin')->check()||Auth::guard('staff')->check()) == false){
            return redirect()->back();
        };
        $data = Barang::where('id',$id)
            ->first();

        return response()->json($data);
    }

    public function edit(Request $request)
    {
        if((Auth::guard('owner')->check()||Auth::guard('admin')->check()||Auth::guard('staff')->check()) == false){
            return redirect()->back();
        };
//        dd($request->all());
        Barang::where('id',$request->id)
            ->update([
                'kode'=>$request->kode,
                'nama'=>$request->nama,
                'stok'=>$request->stok,
                'harga_jual'=>$request->jual,
                'harga_beli'=>$request->beli
            ]);

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        if((Auth::guard('owner')->check()||Auth::guard('admin')->check()||Auth::guard('staff')->check()) == false){
            return redirect()->back();
        };
//        dd($request->all());
        Barang::where('id',$request->id)
            ->delete();

        return redirect()->back();
    }
}
