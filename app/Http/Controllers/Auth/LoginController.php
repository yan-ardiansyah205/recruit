<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\login;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:owner')->except('logout');
        $this->middleware('guest:staff')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }
    public function requestLogin(Request $request)
    {
        switch ($request->role){
            case 1;
                if(Auth::guard('admin')->attempt(['email' => $request->email,'password' => $request->password], $request->get('remember'))){
                    return redirect()->route('home.admin');
                }else{
                    return redirect()->back();
                }
                break;
            case 2;
                if(Auth::guard('staff')->attempt(['email' => $request->email,'password' => $request->password], $request->get('remember'))){
                    return redirect()->route('home.staff');
                }else{
                    return redirect()->back();
                }
                break;
            case 3;
                if(Auth::guard('owner')->attempt(['email' => $request->email,'password' => $request->password], $request->get('remember'))){
                    return redirect()->route('home.owner');
                }else{
                    return redirect()->back();
                }
                break;
        }

    }

}
