<?php

namespace App\Http\Controllers\Api;

use App\Barang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    public function index()
    {
        $data = Barang::select('id','kode','nama','stok','harga_jual','harga_beli')
            ->get();

        return DataTables::of($data)->make();
    }
}
