<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class login
{
//    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        dd($this->request->all());
        switch ($this->request->role){
            case 1;
                Auth::guard('admin')->attempt([
                    'email' => $this->request->email,
                    'password' => $this->request->password
                ], $this->request->get('remember'));
                break;
            case 2;
                Auth::guard('staff')->attempt([
                    'email' => $this->request->email,
                    'password' => $this->request->password
                ], $this->request->get('remember'));
                break;
            case 3;
                Auth::guard('owner')->attempt([
                    'email' => $this->request->email,
                    'password' => $this->request->password
                ], $this->request->get('remember'));
                break;
        }
        dd(Auth::check());
    }
}
