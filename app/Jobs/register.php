<?php

namespace App\Jobs;

use App\Admin;
use App\Owner;
use App\Staff;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Hash;

class register
{
//    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->request->role){
            case 1;
                $admin = Admin::create([
                    'name' => $this->request['name'],
                    'email' => $this->request['email'],
                    'password' => bcrypt($this->request['password']),
                ]);
                break;
            case 2;
                $admin = Staff::create([
                    'name' => $this->request['name'],
                    'email' => $this->request['email'],
                    'password' => bcrypt($this->request['password']),
                ]);
                break;
            case 3;
                $admin = Owner::create([
                    'name' => $this->request['name'],
                    'email' => $this->request['email'],
                    'password' => bcrypt($this->request['password']),
                ]);
                break;
        }

    }
}
